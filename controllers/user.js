const {User} = require('../models')
const {decrypt} = require("../helpers/bcrypt")
const fs = require('fs')
const path = require('path')
const { v4: uuidv4 } = require('uuid')

class UserController {
    static async createUser(req, res) {
        const {name, email, password} = req.body
        const picture = req.files ? req.files.picture : null

        const userId = uuidv4()
        if (picture) {
            picture.mv(path.join(__dirname, '../avatar/')+`${userId}.jpg`, err => {
                if (err) {
                    return res.status(500).send(err)
                }
            })
        }

        const data = {
            id: userId,
            name,
            email,
            password,
            avatarPicture: req.files ? picture : null
        }

        await User.create(data)

        return res.status(201).send('Success: Account has been created')
    }

    static async readAllUser(req, res) {
        const user = await User.findAll()

        return res.status(200).json(user)
    }

    static async readOneUser(req, res) {
        const {id} = req.params

        const user = await User.findOne({where: {id}})

        return res.status(200).json(user)
    }

    static async updateUser(req, res) {
        const {id} = req.params
        const {name, email, password} = req.body
        const picture = req.files ? req.files.picture : null

        if (picture) {
            picture.mv(path.join(__dirname, '../avatar/')+`${id}.jpg`, err => {
                if (err) {
                    return res.status(500).send(err)
                }
            })
        }

        const data = {
            name,
            email,
            password,
            avatarPicture: req.files ? picture : null
        }

        const user = await User.update(data, {where: id})

        return res.status(200).json(user)
    }

    static async deleteUser(req, res) {
        const {id} = req.params

        const user = await User.destroy({where: id})

        return res.status(201).send('Success: Your account has been deleted')
    }
}

module.exports = UserController
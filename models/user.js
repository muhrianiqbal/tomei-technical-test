'use strict';
const {encrypt} = require("../helpers/bcrypt");

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  User.init({
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: true
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    avatarPicture: {
      type: DataTypes.JSON,
    }
  }, {
    sequelize,
    modelName: 'User',
  }, {
    hooks: {
      beforeCreate: (user) => {
        user.password = encrypt(user.password)
      }
    }
  });
  return User;
};
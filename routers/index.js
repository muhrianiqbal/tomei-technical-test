const router = require('express').Router()
const UserController = require('../controllers/user')

// Get
router.get('/user', UserController.readAllUser)
router.get('/user/:id', UserController.readOneUser)

// Post
router.post('/signup', UserController.createUser)

// Patch
router.patch('/update/:id', UserController.updateUser)

// Delete
router.delete('/delete/:id', UserController.deleteUser)

module.exports = router
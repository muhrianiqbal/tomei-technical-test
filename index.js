const express = require("express")
const app = express()
const router = require('./routers')
const cors = require('cors')
const fileUpload = require('express-fileupload')
const port = 3001

app.use(cors())
app.use(express.json({limit: '10mb'}))
app.use(express.urlencoded({limit: '10mb', extended: false}))
app.use(fileUpload({
    createParentPath:true,
    limits:{
    fileSize: 10 * 1024 * 1024
    },
}))

app.use('/', router)

app.listen(port, () => {
    console.log('Server is up on port ', port)
})